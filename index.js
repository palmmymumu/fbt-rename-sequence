const async = require('async')
const express = require('express')
const fs = require('fs')
const app = express()

const SOURCE_DIRECTORY = `${__dirname}/input`
const PORT = 3099

app.listen(PORT)
console.log(`Server is listening on port ${PORT}`)

app.get('/rename', (req, res) => {
	let files = [],
		renamed = {},
		error = 0

	async.series(
		[
			cb => {
				fs.readdir(SOURCE_DIRECTORY, (err, res) => {
					if (err) return cb(err)
					res.forEach(file => {
						if (file.indexOf('_') != -1 && file.indexOf('.') != -1) files.push(file)
					})
					cb()
				})
			},
			cb => {
				async.eachLimit(
					files,
					5000,
					(file, cb) => {
						const uniqueName = file.split('_')[0]
						const ext = file
							.split('.')
							.slice(-1)
							.pop()
						const keyName = `${uniqueName}.${ext}`
						let index = renamed[keyName]
						if (!index) {
							index = 1
							renamed[keyName] = 2
						} else {
							renamed[keyName]++
						}
						const fullName = `${uniqueName}-${index}.${ext}`
						fs.rename(`${SOURCE_DIRECTORY}/${file}`, `${SOURCE_DIRECTORY}/${fullName}`, err => {
							if (err) error++
							cb()
						})
					},
					cb
				)
			}
		],
		err => {
			if (err) return res.json(err)
			res.json({
				sourceFile: files.length,
				uniqueFile: Object.keys(renamed).length,
				error
			})
		}
	)
})
